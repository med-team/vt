Description: Build again external/system libs
  Migration from 1.8 to 1.10 libhts
Author: Antoni Villalonga i Noceras <antoni@friki.cat>
Last-Update: Wed, 18 Jun 2020 09:10:04 +0200
--- a/Makefile
+++ b/Makefile
@@ -1,6 +1,6 @@
 OPTFLAG = -O3
-INCLUDES = -I./lib -I. -I./lib/htslib -I./lib/Rmath -I./lib/pcre2
-CXXFLAGS = -pipe -std=c++0x $(OPTFLAG) $(INCLUDES) -D__STDC_LIMIT_MACROS
+INCLUDES = -I. -I/usr/share/R/include
+CXXFLAGS += -pipe -std=c++0x $(OPTFLAG) $(INCLUDES) -D__STDC_LIMIT_MACROS -DPCRE2_CODE_UNIT_WIDTH=8
 CXX = g++
 
 SOURCESONLY =
@@ -139,52 +139,26 @@
 TARGET = vt
 TOOLSRC = $(SOURCES:=.cpp) $(SOURCESONLY)
 TOOLOBJ = $(TOOLSRC:.cpp=.o)
-LIBDEFLATE = lib/libdeflate/libdeflate.a
-LIBHTS = lib/htslib/libhts.a
-LIBRMATH = lib/Rmath/libRmath.a
-LIBPCRE2 = lib/pcre2/libpcre2.a
-LIBSVM = lib/libsvm/libsvm.a
 
 all : $(TARGET)
 
-${LIBDEFLATE} :
-	cd lib/libdeflate; $(MAKE) || exit 1; 
-	
-${LIBHTS} : ${LIBDEFLATE}
-	export LDFLAGS=-L${PWD}/lib/libdeflate;	export CPPFLAGS=-I${PWD}/lib/libdeflate; cd lib/htslib; autoheader; autoconf; ./configure; $(MAKE) libhts.a || exit 1; 
-
-${LIBRMATH} :
-	cd lib/Rmath; $(MAKE) libRmath.a || exit 1; 
-
-${LIBPCRE2} :
-	cd lib/pcre2; $(MAKE) libpcre2.a || exit 1; 
-
-${LIBSVM} :
-	cd lib/libsvm; $(MAKE) libsvm.a || exit 1; 
-
 version :
 	git rev-parse HEAD | cut -c 1-8 | awk '{print "#define VERSION \"0.5772-"$$0"\""}' > version.h;
 
 $(TARGET) : ${LIBHTS} ${LIBRMATH} ${LIBPCRE2}  ${LIBSVM} $(TOOLOBJ) 
-	$(CXX) $(CXXFLAGS) -o $@ $(TOOLOBJ) $(LIBHTS) $(LIBRMATH) ${LIBPCRE2} ${LIBDEFLATE} -lz -lpthread -lbz2 -llzma -lcurl -lcrypto
+	$(CXX) $(LDFLAGS) $(CPPFLAGS) $(CXXFLAGS) -o $@ $(TOOLOBJ) $(LIBHTS) $(LIBRMATH) ${LIBPCRE2} ${LIBDEFLATE} -lz -lpthread -lbz2 -llzma -lcurl -lcrypto -lhts -lpcre2-8 -lsvm -lRmath -lR
 
 $(TOOLOBJ): $(HEADERSONLY)
 
 .cpp.o :
-	$(CXX) $(CXXFLAGS) -o $@ -c $*.cpp
+	$(CXX) $(LDFLAGS) $(CPPFLAGS) $(CXXFLAGS) -o $@ -c $*.cpp
 
 .PHONY: clean cleanvt test version
 
-clean :
-	cd lib/libdeflate; $(MAKE) clean
-	cd lib/htslib; $(MAKE) clean
-	cd lib/Rmath; $(MAKE) clean
-	cd lib/pcre2; $(MAKE) clean
-	cd lib/libsvm; $(MAKE) clean
-	-rm -rf $(TARGET) $(TOOLOBJ)
+clean : cleanvt
 
 cleanvt :
-	-rm -rf $(TARGET) $(TOOLOBJ)    
+	-rm -rf $(TARGET) $(TOOLOBJ)
 
 test : vt
 	test/test.sh
--- a/bcf_ordered_writer.cpp
+++ b/bcf_ordered_writer.cpp
@@ -22,6 +22,7 @@
 */
 
 #include "bcf_ordered_writer.h"
+#include <algorithm>
 
 BCFOrderedWriter::BCFOrderedWriter(std::string output_vcf_file_name, int32_t window, int32_t compression)
 {
@@ -155,7 +156,7 @@
 
                 if (i==buffer.end())
                 {
-                    int32_t cutoff_pos1 =  std::max(bcf_get_pos1(buffer.front())-window,1);
+                    int32_t cutoff_pos1 =  std::max((int32_t)bcf_get_pos1(buffer.front())-window,1);
                     if (bcf_get_pos1(v)<cutoff_pos1)
                     {
                         fprintf(stderr, "[%s:%d %s] Might not be sorted for window size %d at current record %s:%d < %d (%d [last record] - %d), please increase window size to at least %d.\n",
@@ -251,7 +252,7 @@
     {
         if (buffer.size()>1)
         {
-            int32_t cutoff_pos1 =  std::max(bcf_get_pos1(buffer.front())-window,1);
+            int32_t cutoff_pos1 =  std::max((int32_t)bcf_get_pos1(buffer.front())-window,1);
 
             while (buffer.size()>1)
             {
--- a/consolidate.h
+++ b/consolidate.h
@@ -25,7 +25,7 @@
 #define CONSOLIDATE_VARIANTS_H
 
 #include "program.h"
-#include "Rmath/Rmath.h"
+#include <Rmath.h>
 #include "log_tool.h"
 
 void consolidate(int argc, char **argv);
--- a/discover.h
+++ b/discover.h
@@ -28,9 +28,9 @@
 #include "variant_filter.h"
 #include "pileup.h"
 #include "bam_ordered_reader.h"
-#include "Rmath/Rmath.h"
+#include <Rmath.h>
 #include "log_tool.h"
 
 void discover(int argc, char ** argv);
 
-#endif
\ No newline at end of file
+#endif
--- a/estimator.h
+++ b/estimator.h
@@ -27,7 +27,7 @@
 #include "utils.h"
 #include "hts_utils.h"
 #include "log_tool.h"
-#include "Rmath/Rmath.h"
+#include <Rmath.h>
 
 /**
  * Class containing estimating methods of common features.
@@ -194,4 +194,4 @@
     private:
 };
 
-#endif
\ No newline at end of file
+#endif
--- a/hts_utils.cpp
+++ b/hts_utils.cpp
@@ -22,7 +22,7 @@
 */
 
 #include "hts_utils.h"
-#include "Rmath/Rmath.h"
+#include <Rmath.h>
 
 /********
  *General
@@ -1525,4 +1525,4 @@
     fprintf(stderr,"\n");
 
     va_end(ap);
-}
\ No newline at end of file
+}
--- a/multiallelics_consolidator.h
+++ b/multiallelics_consolidator.h
@@ -28,7 +28,7 @@
 #include "utils.h"
 #include "bcf_ordered_reader.h"
 #include "bcf_ordered_writer.h"
-#include "Rmath/Rmath.h"
+#include <Rmath.h>
 #include "candidate_region_extractor.h"
 #include "candidate_motif_picker.h"
 #include "flank_detector.h"
--- a/svm_train.h
+++ b/svm_train.h
@@ -26,8 +26,8 @@
 
 #include "program.h"
 #include "libsvm/svm.h"
-#include "Rmath/Rmath.h"
+#include <Rmath.h>
 
 void svm_train(int argc, char ** argv);
 
-#endif
\ No newline at end of file
+#endif
--- a/variant_filter.h
+++ b/variant_filter.h
@@ -26,7 +26,7 @@
 
 #include "hts_utils.h"
 #include "utils.h"
-#include "Rmath/Rmath.h"
+#include <Rmath.h>
 
 /**
  * Class for filtering variants in discover
@@ -187,4 +187,4 @@
     uint32_t get_sclip_u_cutoff();
 };
 
-#endif
\ No newline at end of file
+#endif
--- a/vntr_consolidator.h
+++ b/vntr_consolidator.h
@@ -28,7 +28,7 @@
 #include "utils.h"
 #include "bcf_ordered_reader.h"
 #include "bcf_ordered_writer.h"
-#include "Rmath/Rmath.h"
+#include <Rmath.h>
 #include "candidate_region_extractor.h"
 #include "candidate_motif_picker.h"
 #include "flank_detector.h"
@@ -188,4 +188,4 @@
     private:
 };
 
-#endif
\ No newline at end of file
+#endif
--- a/vntr_extractor.h
+++ b/vntr_extractor.h
@@ -28,7 +28,7 @@
 #include "utils.h"
 #include "bcf_ordered_reader.h"
 #include "bcf_ordered_writer.h"
-#include "Rmath/Rmath.h"
+#include <Rmath.h>
 #include "candidate_region_extractor.h"
 #include "candidate_motif_picker.h"
 #include "flank_detector.h"
--- a/hts_utils.h
+++ b/hts_utils.h
@@ -40,9 +40,11 @@
  *FAI UTILS
  **********/
 typedef struct {
+    int id; // faidx_t->name[id] is for this struct.
     int32_t line_len, line_blen;
     int64_t len;
     uint64_t offset;
+    uint64_t qual_offset;
 } faidx1_t;
 
 KHASH_MAP_INIT_STR(s, faidx1_t)
@@ -52,6 +54,7 @@
     int n, m;
     char **name;
     khash_t(s) *hash;
+    enum fai_format_options format;
 };
 
 /**
